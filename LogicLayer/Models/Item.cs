﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicLayer.Models
{
    public class Item
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
