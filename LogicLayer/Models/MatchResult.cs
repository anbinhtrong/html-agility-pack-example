﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicLayer.Models
{
    public class MatchResult
    {
        public DateTime MatchStart { get; set; }
        public string TeamA { get; set; }
        public string Vs { get; set; }
        public string TeamB { get; set; }
        public string Result { get; set; }
    }
}
