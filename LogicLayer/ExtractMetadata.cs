﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using LogicLayer.Models;
using System.Globalization;

namespace LogicLayer
{
    public class ExtractMetadata
    {
        public List<Item> GetMetadata()
        {
            var result = new List<Item>();
            var website = new HtmlWeb();
            var doc = website.Load("http://dantri.vn");
            var metaTags = doc.DocumentNode.SelectNodes("//meta");
            if (metaTags != null)
            {
                foreach (var tag in metaTags)
                {
                    if (tag.Attributes["name"] != null && tag.Attributes["content"] != null)
                    {
                        result.Add(new Item { Key = tag.Attributes["name"].Value, Value = tag.Attributes["content"].Value});

                    }
                }
            }
            return result;
        }

        public List<MatchResult> ExtractDataFromItatenies()
        {
            var result = new List<MatchResult>();
            var website = new HtmlWeb();
            var doc = website.Load("http://itarankings.itatennis.com/LatestResults.aspx");
            var tables = doc.DocumentNode.SelectNodes("//table");
            if (tables != null)
            {
                foreach (var table in tables)
                {
                    if (table.Attributes["id"] != null)
                    {
                        if (table.Attributes["id"].Value != "ctl00_ContentPlaceHolder1_grvTeam") continue;
                    }
                    else
                        continue;
                    var tRows = table.SelectNodes("tr");
                    foreach (var row in tRows)
                    {
                        try
                        {
                            var matchResult = new MatchResult();
                            var nodes = row.SelectNodes("td");
                            if (nodes.Count < 5) continue;
                            matchResult.MatchStart = DateTime.ParseExact(RemoveGarbadgetext(nodes[0].InnerText), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                            matchResult.TeamA = nodes[1].InnerText;
                            matchResult.TeamB = nodes[3].InnerText;
                            matchResult.Vs = nodes[2].InnerText;
                            matchResult.Result = nodes[4].InnerText;
                            result.Add(matchResult);
                        }
                        catch { }
                    }                    
                }
            }
            return result;
        }

        private string RemoveGarbadgetext(string source)
        {
            source = source.Replace("\r\n", "");
            source = source.Trim();
            return source;
        }
    }
}
