﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LogicLayer;

namespace HtmlAgilityPackExample.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var extractMetadata = new ExtractMetadata();
            return View(extractMetadata.GetMetadata());
        }

        public ActionResult ExtractDataFromITA()
        {
            var extractMetadata = new ExtractMetadata();
            return View(extractMetadata.ExtractDataFromItatenies());
        }

    }
}
